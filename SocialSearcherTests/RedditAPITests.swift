//
//  RedditAPITests.swift
//  SocialSearcherTests
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import XCTest
@testable import SocialSearcher

class RedditAPITests: XCTestCase {
    let successSession: MockURLSession = {
        let session = MockURLSession()
        let dataPath = Bundle(for: RedditAPITests.self).path(forResource: "search", ofType: "json")
        let dataUrl = URL(fileURLWithPath: dataPath ?? "")
        session.data = try? Data(contentsOf: dataUrl)
        return session
    }()
    
    func test_search_listingDecodeSuccess() {
        let api = RedditAPI(session: successSession)
        
        api.search(query: "test") { (listing, error) in
            XCTAssertNotNil(listing)
            XCTAssertNil(error)
        }
    }
    
    func test_search_queryStringIsFormed() {
        let api = RedditAPI(session: successSession)
        
        api.search(query: "cats") { (listing, error) in }
        XCTAssertEqual(successSession.urlCapture?.absoluteString, "https://www.reddit.com/search.json?raw_json=1&q=cats&after")
    }
    
    func test_search_badDataResultsInError() {
        let session = MockURLSession()
        session.data = Data()
        let api = RedditAPI(session: session)
        
        api.search(query: "dogs") { (listing, error) in
            XCTAssertNotNil(error)
        }
    }
}

class MockURLSession: URLSession {
    var data: Data?
    var urlResponse: URLResponse?
    var error: Error?
    
    var urlCapture: URL?
    
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        urlCapture = url
        let dataTask = MockURLSessionDataTask(withData: data, urlResponse: urlResponse, error: error, completionHandler: completionHandler)
        return dataTask
    }
}

class MockURLSessionDataTask: URLSessionDataTask {
    let completion: (Data?, URLResponse?, Error?) -> Void
    
    var completionData: Data?
    var completionUrlResponse: URLResponse?
    var completionError: Error?
    
    init(withData data: Data?, urlResponse: URLResponse?, error: Error?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        completionData = data
        completionUrlResponse = urlResponse
        completionError = error
        completion = completionHandler
    }
    
    override func resume() {
        completion(completionData, completionUrlResponse, completionError)
    }
}
