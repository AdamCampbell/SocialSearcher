//
//  RedditSearchPresenterTests.swift
//  SocialSearcherTests
//
//  Created by Adam Campbell on 15/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import XCTest
@testable import SocialSearcher

class RedditSearchPresenterTests: XCTestCase {
    var api: MockRedditAPI!
    var viewSurface: MockRedditSearchViewSurface!
    var presenter: RedditSearchPresenter!
    
    override func setUp() {
        api = MockRedditAPI()
        viewSurface = MockRedditSearchViewSurface()
        presenter = RedditSearchPresenter(api: api)
        presenter.attach(toViewSurface: viewSurface)
    }
    
    func test_didSearchForQueryString_callsSearchOnAPI_withPassedQuery() {
        presenter.didSearchFor(searchQuery: "cats")
        
        XCTAssertTrue(api.didSearch)
        XCTAssertEqual(api.capturedQuery, "cats")
    }
    
    func test_didSearchForQueryString_callsSearchOnAPI_doesntSendAnAfterToken() {
        presenter.didSearchFor(searchQuery: "cats")
        
        XCTAssertNil(api.capturedAfter)
    }
    
    func test_didSearchForQueryString_whenSearchReturnsWithAListing_tellsTheViewSurfaceToDisplay() {
        presenter.didSearchFor(searchQuery: "cats")
        let listing = Listing(after: nil, children: [postTestFixture()])
        
        api.capturedCompletion?(listing, nil)
        
        XCTAssertTrue(viewSurface.didDisplayPosts)
        XCTAssertTrue(viewSurface.capturedPosts.count == 1)
        XCTAssertEqual(viewSurface.capturedPosts.first?.data.title, "carrot")
    }
    
    func test_didSearchForQueryString_whenSearchDoesntReturnAListing_doesntTellTheViewSurfaceToDisplay() {
        presenter.didSearchFor(searchQuery: "cats")
        
        api.capturedCompletion?(nil, nil)
        
        XCTAssertFalse(viewSurface.didDisplayPosts)
    }
    
    func test_willScrollToEnd_callsSearchOnAPI_passingTheExpectedParameters() {
        presenter.willScrollToEnd(ofSearchQuery: "dogs", withName: "abc")
        
        XCTAssertTrue(api.didSearch)
        XCTAssertEqual(api.capturedQuery, "dogs")
        XCTAssertEqual(api.capturedAfter, "abc")
    }
    
    func test_willScrollToEnd_whenSearchReturnsWithAListing_tellsTheViewSurfaceToDisplaysAdditional() {
        presenter.willScrollToEnd(ofSearchQuery: "dogs", withName: "abc")
        let listing = Listing(after: nil, children: [postTestFixture()])
        
        api.capturedCompletion?(listing, nil)
        
        XCTAssertTrue(viewSurface.didDisplayAdditionalPosts)
        XCTAssertTrue(viewSurface.capturedAdditionalPosts.count == 1)
        XCTAssertEqual(viewSurface.capturedAdditionalPosts.first?.data.title, "carrot")
    }
    
    func test_willScrollToEnd_whenSearchDoesntReturnWithAListing_doesntTellTheViewSurfaceToDisplaysAdditional() {
        presenter.willScrollToEnd(ofSearchQuery: "dogs", withName: "abc")

        api.capturedCompletion?(nil, nil)
        
        XCTAssertFalse(viewSurface.didDisplayAdditionalPosts)
    }
    
    func test_didTapPost_displaysAWebViewForThePostUrl() {
        let post = postTestFixture()
        presenter.didTap(post: post)
        
        XCTAssertTrue(viewSurface.didDisplayWebView)
        XCTAssertEqual(viewSurface.capturedWebViewUrl?.absoluteString, "https://sample.test")
    }
    
    func test_willPresentPostInViewSurface() {
        let postViewSurface = MockPostViewSurface()
        let post = postTestFixture()
        
        presenter.willPresent(post: post, inViewSurface: postViewSurface)
        
        XCTAssertTrue(postViewSurface.didDisplay)
        XCTAssertEqual(postViewSurface.capturedTitle, "carrot")
        XCTAssertEqual(postViewSurface.capturedImageUrl?.absoluteString, "https://firstImage.test")
    }
    
    func postTestFixture() -> Post {
        let url = URL(string:"https://sample.test")!
        let sourceImageUrl = URL(string:"https://sourceImage.test")!
        let firstImageUrl = URL(string:"https://firstImage.test")!
        let sourceResolution = PostImageResolution(url: sourceImageUrl, width: 50, height: 100)
        let firstImageResolution = PostImageResolution(url: firstImageUrl, width: 50, height: 100)
        let image = PostImage(source: sourceResolution, resolutions: [firstImageResolution])
        let post = Post(kind: "testPost", data: PostData(title: "carrot", name: "penelope", preview: PostPreview(images: [image]), url: url))
        return post
    }
}

class MockPostViewSurface: PostViewSurface {
    var didDisplay = false
    var capturedTitle = ""
    var capturedImageUrl: URL?
    
    func display(title: String, imageForUrl url: URL?) {
        didDisplay = true
        capturedTitle = title
        capturedImageUrl = url
    }
}

class MockRedditSearchViewSurface: RedditSearchViewSurface {
    var didDisplayWebView = false
    var capturedWebViewUrl: URL?
    func displayWebView(forUrl url: URL) {
        didDisplayWebView = true
        capturedWebViewUrl = url
    }
    
    var didDisplayPosts = false
    var capturedPosts: [Post] = []
    func displayPosts(posts: [Post]) {
        didDisplayPosts = true
        capturedPosts = posts
    }
    
    var didDisplayAdditionalPosts = false
    var capturedAdditionalPosts: [Post] = []
    func displayAdditionalPosts(posts: [Post]) {
        didDisplayAdditionalPosts = true
        capturedAdditionalPosts = posts
    }
}

class MockRedditAPI: RedditAPI {
    var didSearch = false
    var capturedQuery: String?
    var capturedAfter: String?
    var capturedCompletion: ((Listing?, Error?) -> ())?
    
    override func search(query: String, after: String?, completion: @escaping (Listing?, Error?) -> ()) {
        didSearch = true
        capturedQuery = query
        capturedAfter = after
        capturedCompletion = completion
    }
}
