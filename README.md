# Social Searcher

A lightweight reddit searcher using Reddit's public https API

## Running the Project

The project uses a `.xcodeproj` format compatible with `Xcode 9.3` and up and uses `Carthage` for it's dependencies.

### Setup

1. Install `Xcode 9.3` (if not already install)
2. In `Xcode Prefereces` > `Locations` set the Xcode command line tools to version `Xcode 9.3`
3. Install Carthage either via homebrew or using the latest [release](https://github.com/Carthage/Carthage/releases)
4. In the project directory run Carthage bootstrap
   `carthage bootstrap --platform iOS`
5. Open the `SocialSearcher.xcodeproj` 
6. Run/Build against your preffered target