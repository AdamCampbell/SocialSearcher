//
//  RedditSearchPresenter.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import Foundation

class RedditSearchPresenter {
    private weak var viewSurface: RedditSearchViewSurface?
    private let api: RedditAPI
    
    init(api: RedditAPI = RedditAPI()) {
        self.api = api
    }
    
    func attach(toViewSurface viewSurface: RedditSearchViewSurface) {
        self.viewSurface = viewSurface
    }
    
    func didSearchFor(searchQuery: String) {
        api.search(query: searchQuery) { [weak self] (listing, error) in
            guard let posts = listing?.children, posts.count > 0 else {
                return
            }
            
            self?.viewSurface?.displayPosts(posts: posts)
        }
    }
    
    func didTap(post: Post) {
        viewSurface?.displayWebView(forUrl: post.data.url)
    }
    
    func willPresent(post: Post, inViewSurface viewSurface: PostViewSurface) {
        viewSurface.display(title: post.data.title, imageForUrl: post.data.preview?.images.first?.resolutions.first?.url)
    }
    
    func willScrollToEnd(ofSearchQuery searchQuery: String, withName name: String) {
        api.search(query: searchQuery, after: name) { [weak self] (listing, error) in
            guard let posts = listing?.children, posts.count > 0 else {
                return
            }
            
            self?.viewSurface?.displayAdditionalPosts(posts: posts)
        }
    }
}
