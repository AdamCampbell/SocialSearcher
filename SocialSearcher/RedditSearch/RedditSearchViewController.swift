//
//  RedditSearchViewController.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import UIKit
import SafariServices

protocol RedditSearchViewSurface: AnyObject {
    func displayPosts(posts: [Post])
    func displayAdditionalPosts(posts: [Post])
    func displayWebView(forUrl url: URL)
}

class RedditSearchViewController: UIViewController {
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var resultsTableView: UITableView!
    
    private let presenter = RedditSearchPresenter()
    private let postCellIdentifier = String(describing: PostTableViewCell.self)
    private var data: [Post] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.resultsTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
        let nib = UINib(nibName: postCellIdentifier, bundle: Bundle.main)
        resultsTableView.register(nib, forCellReuseIdentifier: postCellIdentifier)
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
        
        presenter.attach(toViewSurface: self)
    }
}

extension RedditSearchViewController: RedditSearchViewSurface {
    func displayPosts(posts: [Post]) {
        data = posts
    }
    
    func displayAdditionalPosts(posts: [Post]) {
        data += posts
    }
    
    func displayWebView(forUrl url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        present(safariViewController, animated: true, completion: nil)
    }
}

extension RedditSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchQuery = searchBar.text else {
            return
        }
        
        presenter.didSearchFor(searchQuery: searchQuery)
        searchBar.resignFirstResponder()
    }
}

extension RedditSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: postCellIdentifier) as? PostTableViewCell else {
            return UITableViewCell()
        }
        
        presenter.willPresent(post: data[indexPath.row], inViewSurface: cell)
        
        return cell
    }
}

extension RedditSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didTap(post: data[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let searchQuery = searchBar.text, let lastPost = data.last, indexPath.row == data.endIndex - 1 {
            presenter.willScrollToEnd(ofSearchQuery: searchQuery, withName: lastPost.data.name)
        }
    }
}
