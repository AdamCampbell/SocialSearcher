//
//  PostTableViewCell.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 15/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import UIKit
import Kingfisher

protocol PostViewSurface {
    func display(title: String, imageForUrl url: URL?)
}

class PostTableViewCell: UITableViewCell {
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var previewImageView: UIImageView!
    
    private var previewImageUrl: URL? {
        didSet {
            if let url = previewImageUrl {
                previewImageView.isHidden = false
                previewImageView.kf.setImage(with: url)
            } else {
                previewImageView.isHidden = true
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        previewImageView.image = nil
    }
}

extension PostTableViewCell: PostViewSurface {
    func display(title: String, imageForUrl url: URL?) {
        titleLabel.text = title
        previewImageUrl = url
    }
}
