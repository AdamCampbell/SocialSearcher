//
//  Post.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import Foundation

struct Post: Codable {
    let kind: String
    let data: PostData
}

struct PostData: Codable {
    let title: String
    let name: String
    let preview: PostPreview?
    let url: URL
}

struct PostPreview: Codable {
    let images: [PostImage]
}

struct PostImage: Codable {
    let source: PostImageResolution
    let resolutions: [PostImageResolution]
}

struct PostImageResolution: Codable {
    let url: URL
    let width: Int
    let height: Int
}
