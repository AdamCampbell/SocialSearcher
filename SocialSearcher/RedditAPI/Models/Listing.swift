//
//  Listing.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import Foundation

struct Listing: Codable {
    let after: String?
    let children: [Post]
}
