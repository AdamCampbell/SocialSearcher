//
//  RedditAPI.swift
//  SocialSearcher
//
//  Created by Adam Campbell on 14/4/18.
//  Copyright © 2018 Adam Campbell. All rights reserved.
//

import Foundation

class RedditAPI {
    private let session: URLSession
    let redditSearchBaseURL = "https://www.reddit.com/search.json"
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func search(query: String, after: String? = nil, completion: @escaping (Listing?, Error?) -> ()) {
        guard let url: URL = {
            var components = URLComponents(string: redditSearchBaseURL)
            components?.queryItems = [
                URLQueryItem(name: "raw_json", value: "1"),
                URLQueryItem(name: "q", value: query),
                URLQueryItem(name: "after", value: after)
            ]
            return components?.url
        }() else {
            return
        }
        
        session.dataTask(with: url) { (data, response, error) in
            let decoder = JSONDecoder()
            var listing: Listing? = nil
            var error = error
            
            if let data = data {
                do {
                    let response = try decoder.decode(Response<Listing>.self, from: data)
                    listing = response.data
                } catch let decodingError {
                    error = decodingError
                }
            }
            
            completion(listing, error)
        }.resume()
    }
}
